console.log("Hello World!");

// What are conditional statements?

	// Conditional statements allow us to control the flow of our program.
	// Allow us to run statement/instruction if a condition is met or run another separate instruction if not.
	
// [SECTION] If, else if, and else statements
	
	let numA = -1;

	/*
		"if" statement

			-it will execute the statement/code block if a specified condition is met/true.

		Syntax: 
		if(condition){
			statement;
		}
	*/

	if(numA < 0) {
		console.log("Hello from numA");
	}


	// Lets reassign the variable numA and run an if statement with the same condition.

	numA = 1;

	if(numA < 0) {
		console.log("Hello from the reassigned value of numA");
	}

	let city = "New York";

	if(city === "New York"){
		console.log("Welcome to New York!")
	}


	/*
		"else if" statement

			-Executes a statement if previous conditions are false and if the specified condition is true
			-The 'else if' is optional and can be added to capture additional conditions to change the flow of a program.
	*/

	let numH = 1;

	if(numH < 0) {
		console.log("Hello from num(numH < 0).");
	}

	else if(numH > 0) {
		console.log("Hello from num(numH > 0).");
	}

	// else if is dependent with if, you cannot use else if clause alone.
	// if the if() condition was passed and run, we will no longer evaluate else if() and aend the process.


	city = "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York!")
	}

	else if(city === "Toyko"){
		console.log("Welcome to Toyko!")
	}


	/*
		"else" statement

			- Executes a statement if all other conditions are false/not met.
			- Optional and can be added to capture any other result to change the flow of program. 
	*/

	numH = 2;

	if(numH < 0) {
		console.log("Hello from if");
	}

	else if(numH > 2) {
		console.log("Hello from else if");
	}

	else if(numH > 3) {
		console.log("Hello from if");
	}

	else {
		console.log("Hello from else");
	}

	// Since all of the previous if and else conditions were not met, the else statement was executed instead.



	// if, else if, and else statement with functions

	/*
		most of the times we would like to use if, else, and else statement with functions to control the flow of our application
	*/

	let message;

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 0){
			return "Invalid argument!";
		}

		else if(windSpeed <= 30){
			return "Not a typhoon yet!";
		}

		else if(windSpeed <= 60){
			return "Tropical depression!";
		}

		else if(windSpeed <= 88){
			return "Tropical Storm detected!";
		}

		else if(windSpeed <= 117){
			return "Severe Tropical Storm detected!";
		}

		else if(windSpeed >= 118){
			return "Typhoon detected!";
		}

		else {
			return "Invalid argument";
		}
	}
		// Mini Activity
		/*
			1. add return "Tropical Storm Detected" if the windspeed is between 60 and 89
			2. add return "Severe Tropical Storm Detected" if the windspeed is between 90 and 118
			3. if higher than 117, return "Typhoon Detected"
		*/
	console.log(determineTyphoonIntensity("q"));

		message = determineTyphoonIntensity(119);

		if(message === "Typhoon detected!"){
			console.warn(message);
				// console.warn() is a good way to print warning in our console that could help us developers act on certain output within our code.
		}

// [SECTION] Truthy or Falsy
	// Javascript a "truthy" is a value that is considered true when encountered in Boolean context.
	// Values are considered true unless defined otherwise.

	// False values/ exceptions for truthy
		/*
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
		*/
	if(true) {
		console.log("true");
	}

	if(1) {
		console.log("true");
	}

	if(null) {
		console.log("Hello from null inside the if condition");
	}

	else {
		console.log("Hello from null inside the else condition");
	}


// [SECTION] Condition Operator/Ternary Operator
	/*
		The Conditional Operator
		1. condition
		2. expression to execute if the condition is true or truthy
		3. expression if the condition is falsy

		Syntax:
		(expression) ? ifTrue : ifFalse;
	*/

	let ternaryResult = (1 > 18) ? 1 : 2;
	console.log(ternaryResult);


// [SECTION] Switch Statement
	/*
		The switch statement evaluates an expression and matches the expression's value to a case class.
	*/

		// ".toLowerCase" will convert all the letter to lower case
	let day = prompt("What is the day of the week today?").toLowerCase();

	switch(day) {
		case 'monday' :
			console.log('The color of the day is red');
			// the code will stop here
			break;

		case 'tuesday' :
			console.log('The color of the day is orange');
			break;

		case 'wednesday' :
			console.log('The color of the day is yellow');
			break;

		case 'thursday' :
			console.log('The color of the day is green');
			break;

		case 'friday' :
			console.log('The color of the day is blue');
			break;

		case 'saturday' :
			console.log('The color of the day is indigo');
			break;

		case 'sunday' :
			console.log('The color of the day is purple');
			break;

		default :
			console.log("Please input a valid day");
			break;
	}


// [SECTION] Try-Catch-Finally
	/*
		-for error handling
		-there are instances when an application returns an error/warning that is not necessarily an error in the context of our code.
		-these errors are result of an attempt of the programming language to help developers in creating efficient code.
	*/

	function showIntensity(windSpeed) {
		try {
			// codes that will be executed or run
			alert(determineTyphoonIntensity(windSpeed));
			alert("Intensity updates will show alert from try.");
		}

		catch(error) {
			console.warn(error.message);
		}

		finally{
			// continue execution of code regardless of success or failure of code execution in the try statement
			alert("Intensity updates will show new alert from finally.")
		}
	}

	showIntensity(119);